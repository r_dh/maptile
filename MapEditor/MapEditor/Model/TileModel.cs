﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Ioc;

namespace MapEditor.Model {
    [Serializable]
    public class TileModel : ObservableObject {

        //id
        private uint _id;
        public uint Id {
            get { return _id; }
            set {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        //scaling
        private double _scale;
        public double Scale {
            get { return _scale; }
            set {
                _scale = value;
                RaisePropertyChanged("Scale");
            }
        }

        //pos x
        private int _posX;
        public int PosX {
            get { return _posX; }
            set {
                _posX = value;
                RaisePropertyChanged("PosX");
            }
        }

        //pos y
        private int _posY;
        public int PosY {
            get { return _posY; }
            set {
                _posY = value;
                RaisePropertyChanged("PosY");
            }
        }

        //width
        private double _width;
        public double Width {
            get { return _width; }
            set {
                _width = value;
                RaisePropertyChanged("Width");
            }
        }

        //height
        private double _height;
        public double Height {
            get { return _height; }
            set {
                _height = value;
                RaisePropertyChanged("Height");
            }
        }

        //image
        //used to be ImageSource instead of BitmapImage
        private BitmapImage _mapTileImage;
        public BitmapImage MapTileImage {
            get { return _mapTileImage; }
            set {
                _mapTileImage = value;
                RaisePropertyChanged("MapTileImage");
            }
        }


        public void CopyFrom(TileModel source) {
            MapTileImage = source._mapTileImage;
            Id = source.Id;
            Scale = source._scale;
            PosX = source._posX;
            PosY = source._posY;
            Width = source._width;
            Height = source._height;
        }

        public TileModel Copy() {
            var newTile = new TileModel() {
                MapTileImage = this._mapTileImage,
                Id = this.Id,
                Scale = this._scale,
                PosX = this._posX,
                PosY = this._posY,
                Width = this._width,
                Height = this._height
            };

            return newTile;
        }

        public TileModel() {
            
        }
    }
}
