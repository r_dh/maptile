﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using GalaSoft.MvvmLight;
using MapEditor.Helpers;

namespace MapEditor.Model {
    [Serializable]
    public class LayerModel : ObservableObject {
        public ObservableCollection<TileModel> TileGrid { get; set; }

        private string _layerName = "Nameless layer";
        public string LayerName {
            get { return _layerName; }
            set { _layerName = value; }
        }

        private uint _id;
        public uint Id {
            get { return _id; }
            set {
                _id = value;
                RaisePropertyChanged("Id");
            }
        }

        private bool _enabled;
        public bool Enabled {
            get { return _enabled; }
            set {
                _enabled = value;
                RaisePropertyChanged("Enabled");
            }
        }

        public void SetImageAt(uint id, BitmapImage image) {
            TileGrid.ElementAt((int)id).MapTileImage = image;
        }

        public void ResetImages() {
            BitmapImage emptyImg = BitmapConverter.ToBitmapImage(BitmapConverter.EmptyTile(32));
            foreach (var tile in TileGrid) {
                tile.MapTileImage = emptyImg; //null
            }
        }

        public LayerModel Copy() {
            LayerModel newLayerModel = new LayerModel() {
                LayerName = this._layerName,
                Id = this._id,
                Enabled = this._enabled,
                TileGrid = this.TileGrid
            };

            return newLayerModel;
        }

        public LayerModel() {
            
        }
    }
}
