﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;
using MapEditor.Model;

namespace MapEditor.ViewModel {
    class LoadBitmapViewModel : ObservableObject {

        private TileModel _bitmapImage;
        public TileModel BitmapImage {
            get { return _bitmapImage; }
            set {
                _bitmapImage = value;
                 RaisePropertyChanged("BitmapImage");
                //  _contact.PropertyChanged += (sender, args) => ApplyEditsCommand.RaiseCanExecuteChanged();
            }
        }
    }
}
