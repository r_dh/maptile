using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Serialization;
using GalaSoft.MvvmLight;
//using GalaSoft.MvvmLight.Command;
//using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.CommandWpf;
using MapEditor.Model;
using MapEditor.Helpers;
using Microsoft.Win32;
using Rectangle = System.Windows.Shapes.Rectangle;

namespace MapEditor.ViewModel {

    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase {

        public ObservableCollection<TileModel> MapTiles { get; private set; }
        public TileModel SelectedMapTile { get; set; }
        public ObservableCollection<TileModel> MapBackground { get; private set; }
        public ObservableCollection<LayerModel> MapLayers { get; private set; }
        public LayerModel SelectedMapLayer { get; set; }

        const int SPRITE_SIZE = 32;
        int _tileCountX = 16, _tileCountY = 16;//todo grid size
        private float _zoomLevel = 1f;
        private bool _mouseDown = false;

        public int MapWidth {
            get { return _tileCountX * SPRITE_SIZE; }
            private set { _tileCountX = value / SPRITE_SIZE; RaisePropertyChanged("MapWidth"); }//todo set grid size
        }
        public int MapHeight {
            get { return _tileCountY * SPRITE_SIZE; }
            private set { _tileCountY = value / SPRITE_SIZE; RaisePropertyChanged("MapHeight"); }
        }
      
        public float ZoomLevel {
            get { return _zoomLevel; }
            set { _zoomLevel = value; RaisePropertyChanged("ZoomLevel"); }
        }

        public bool LoadingBarDeterminate {
            get { return _percent != 0 && _percent != 100; }
            set { throw new NotImplementedException(); }
        }

        private bool Initialized = false;
        private readonly BackgroundWorker _worker = new BackgroundWorker();

        private int _percent = 0;
        public int StatusProgressPercentage { get { return _percent; }
            set { _percent = value; RaisePropertyChanged("StatusProgressPercentage"); RaisePropertyChanged("LoadingBarDeterminate"); }
        }
        public string StatusText { get; set; }

        private Stack<SpriteAction> _undoStack = new Stack<SpriteAction>();
        private Stack<SpriteAction> _redoStack = new Stack<SpriteAction>();
        private long _currentActionIndex = 0;
        private int _actionCounter = 0;

        #region Commands
        private RelayCommand _loadSpriteCommand;
        public RelayCommand LoadSpriteCommand {
            get { return _loadSpriteCommand ?? (_loadSpriteCommand = new RelayCommand(LoadSprite)); }
        }

        private RelayCommand _loadSpriteSheetCommand;
        public RelayCommand LoadSpriteSheetCommand {
            get { return _loadSpriteSheetCommand ?? (_loadSpriteSheetCommand = new RelayCommand(LoadSpriteSheet)); }
        }

        private RelayCommand<TileModel> _changeTileCommand;
        public RelayCommand<TileModel> ChangeTilesCommand {
            get { return _changeTileCommand ?? (_changeTileCommand = new RelayCommand<TileModel>(UpdateTile, CanDraw)); }
        }

        private RelayCommand<MouseEventArgs> _drawingCommand;
        public RelayCommand<MouseEventArgs> DrawingCommand {
            get { return _drawingCommand ?? (_drawingCommand = new RelayCommand<MouseEventArgs>(MouseDown)); }
        }

        private RelayCommand<MouseWheelEventArgs> _zoomCommand;
        public RelayCommand<MouseWheelEventArgs> ZoomCommand {
            get { return _zoomCommand ?? (_zoomCommand = new RelayCommand<MouseWheelEventArgs>(ScrollZoom)); }
        }

        private RelayCommand<float> _zoomSetCommand;
        public RelayCommand<float> ZoomSetCommand {
            get { return _zoomSetCommand ?? (_zoomSetCommand = new RelayCommand<float>(SetZoom)); }
        }

        private RelayCommand _addLayerCommand;
        public RelayCommand AddLayerCommand {
            get { return _addLayerCommand ?? (_addLayerCommand = new RelayCommand(AddLayer)); }
        }

        private RelayCommand _removeLayerCommand;
        public RelayCommand RemoveLayerCommand {
            get { return _removeLayerCommand ?? (_removeLayerCommand = new RelayCommand(RemoveLayer)); }
        }

        private RelayCommand _saveImageCommand;
        public RelayCommand SaveImageCommand {
            get { return _saveImageCommand ?? (_saveImageCommand = new RelayCommand(SaveImage)); }
        }

        private RelayCommand _saveCommand;
        public RelayCommand SaveCommand {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
        }

        private RelayCommand _loadCommand;
        public RelayCommand LoadCommand {
            get { return _loadCommand ?? (_loadCommand = new RelayCommand(Load)); }
        }

        private RelayCommand _undoCommand;
        public RelayCommand UndoCommand {
            get { return _undoCommand ?? (_undoCommand = new RelayCommand(Undo)); }
        }

        private RelayCommand _redoCommand;
        public RelayCommand RedoCommand {
            get { return _redoCommand ?? (_redoCommand = new RelayCommand(Redo)); }
        }

        private RelayCommand _resetCommand;
        public RelayCommand ResetCommand {
            get { return _resetCommand ?? (_resetCommand = new RelayCommand(Reset)); }
        }

        private RelayCommand _importCommand;
        public RelayCommand ImportCommand {
            get { return _importCommand ?? (_importCommand = new RelayCommand(Import)); }
        }

        private RelayCommand<int> _resizeCommand;
        public RelayCommand<int> ResizeCommand {
            get { return _resizeCommand ?? (_resizeCommand = new RelayCommand<int>(GridResizeUniform)); }
        }

        private RelayCommand _eraseCommand;
        public RelayCommand EraseCommand {
            get { return _eraseCommand ?? (_eraseCommand = new RelayCommand(Eraser)); }
        }

        private RelayCommand _fillCommand;
        public RelayCommand FillCommand {
            get { return _fillCommand ?? (_fillCommand = new RelayCommand(Fill)); }
        }
        #endregion endregion

        public MainViewModel() {
            MapTiles = new ObservableCollection<TileModel>();
            MapBackground = new ObservableCollection<TileModel>();
            MapLayers = new ObservableCollection<LayerModel>();

            _worker.WorkerReportsProgress = true;
            _worker.DoWork += WorkerDoWork;
            _worker.ProgressChanged += ProgressChanged;
            _worker.RunWorkerCompleted += WorkerRunWorkerCompleted;

            SetStatus("Initatializing layout", 0);

            InitLayout(_tileCountX, _tileCountY);

            MapLayers.Add(new LayerModel { TileGrid = MapBackground, Id = 0, LayerName = "BackgroundLayer", Enabled = true });

            SelectedMapLayer = MapLayers[0];

            SetStatus("Load a file to start", 0);
        }

        private void InitLayout(int columns, int rows) {

            MapBackground.Clear();
            var image = new BitmapImage();

            //Init with first bitmap
            if (MapTiles.Count > 0) image = MapTiles[0].MapTileImage;
            //Init with empty bitmap
            image = BitmapConverter.ToBitmapImage(BitmapConverter.EmptyTile(SPRITE_SIZE));

            for (int row = 0; row < rows; row++) {
                for (int column = 0; column < columns; column++) {

                    MapBackground.Add(new TileModel {
                        Id = (uint)(row * columns + column),
                        MapTileImage = image,
                        Height = SPRITE_SIZE,
                        Width = SPRITE_SIZE,
                        PosX = column * SPRITE_SIZE,
                        PosY = row * SPRITE_SIZE,
                        Scale = 1
                    });
                }
            }
        }

        public void LoadSprite() {
 
            var openFile = new OpenFileDialog();

            openFile.Title = "Select image file(s)";
            openFile.Multiselect = true;
           
            openFile.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";

            SetStatus("Pick sprite(s)", 0);
            var result = openFile.ShowDialog();

            if (result == true) {
                Random r = new Random();
                foreach (string file in openFile.FileNames) {
                    try {
                        var img = new BitmapImage(new Uri(file));

                        var x = new TileModel {
                            Id = (uint)r.Next(9999999),
                            MapTileImage = img,
                            Height = img.Height,
                            Width = img.Width,
                            PosX = 0,
                            PosY = 0,
                            Scale = 1
                        };

                        MapTiles.Add(x);
                    } catch (Exception ex) {
                        // Could not load the image.
                        MessageBox.Show("Could not load image: " + file.Substring(file.LastIndexOf('\\')) + "\n\nReported error: " + ex.Message);
                    }
                }
            }

            if (!Initialized) InitLayout(_tileCountX, _tileCountY);
            Initialized = true;
            SetStatus("Ready", 100);
        }

        public void LoadSpriteSheet() {
            //_worker.RunWorkerAsync();
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.Title = "Select image file";
            
            openFile.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";

            SetStatus("Pick spritesheet", 0);
            var result = openFile.ShowDialog();

            if (result == true) {
                MapTiles.Clear();

                //BitmapImage spriteSheet = new BitmapImage(new Uri(openFile.FileName));
                BitmapImage spriteSheet = BitmapConverter.ToBitmapImage(new Bitmap(openFile.FileName));
                Rectangle cropRectangle = new Rectangle();

                spriteSheet = BitmapConverter.BitmapSourceToImage(BitmapConverter.ConvertBitmapTo96DPI(spriteSheet));

                int xSprites = (int)spriteSheet.Width / SPRITE_SIZE;
                int ySprites = (int)spriteSheet.Height / SPRITE_SIZE;

                SetStatus("Extracting spritesheet", 0);////  250/15323 sprites parsed
                //Random r = new Random();

                // The parameters you want to pass to the do work event of the background worker.
                object[] parameters = new object[] { xSprites, ySprites, spriteSheet, openFile };

                // This runs the event on new background worker thread.
                _worker.RunWorkerAsync(parameters);

                //SetStatus((xSprites * ySprites) + " tiles parsed", 100);
            }
        }

        void UpdateTile(TileModel tile) {
            if (tile == null) return;
            if (SelectedMapTile == null) return;
            //int i = MapLayers.IndexOf(SelectedMapLayer);
            if (SelectedMapLayer == null) SelectedMapLayer = MapLayers[0];
            LayerModel selectedLayer = MapLayers.ElementAt(MapLayers.IndexOf(SelectedMapLayer)); //Selected layermodel
            TileModel selectedTile = selectedLayer.TileGrid.ElementAt((int)(tile.Id)); //Selected tile (within right layer)

            BitmapImage prevBitmap = tile.MapTileImage;
            BitmapImage next = selectedLayer.TileGrid.ElementAt((int)(tile.Id)).MapTileImage;
            
            _undoStack.Push(new SpriteAction(selectedTile, selectedTile.MapTileImage, SelectedMapTile.MapTileImage, _actionCounter > 0 ? _currentActionIndex - 1 : _currentActionIndex++));

            //_undoStack.Push(new SpriteAction(tile, tile.MapTileImage, SelectedMapTile.MapTileImage, _actionCounter > 0 ? _currentActionIndex - 1 : _currentActionIndex++));

            MapLayers.ElementAt(MapLayers.IndexOf(SelectedMapLayer)).SetImageAt(tile.Id, SelectedMapTile.MapTileImage);

            ++_actionCounter;
            //tile.MapTileImage = SelectedMapTile.MapTileImage;
        }

        void MouseDown(MouseEventArgs mouse) {
            if (mouse.LeftButton == MouseButtonState.Pressed) { _mouseDown = true; } else if (mouse.LeftButton == MouseButtonState.Released) {
                _actionCounter = 0;
                _mouseDown = false;
            }
        }

        void ScrollZoom(MouseWheelEventArgs args) {
            _zoomLevel += (args.Delta > 0) ? 0.1f : -0.1f;
            _zoomLevel = (_zoomLevel < 0.5f) ? 0.5f : (_zoomLevel > 4f) ? 4f : _zoomLevel;
            RaisePropertyChanged("ZoomLevel");
        }

        void SetZoom(float zoom) {
            _zoomLevel = (zoom < 0.5f) ? 0.5f : (zoom > 4f) ? 4f : zoom;
            RaisePropertyChanged("ZoomLevel");
        }

        bool CanDraw(TileModel tile) {
            return _mouseDown;
        }

        void AddLayer() {
            int index = MapLayers.Count();

            LayerModel layerModel = new LayerModel {
                TileGrid = new ObservableCollection<TileModel>(),
                Id = (uint)index,
                LayerName = $"Layer{index}",
                Enabled = true
            };

            foreach (TileModel tile in MapBackground) {
                layerModel.TileGrid.Add(tile.Copy());
            }

            MapLayers.Add(layerModel.Copy());
            MapLayers.ElementAt(index).ResetImages();
            _undoStack.Push(new SpriteAction(MapLayers.ElementAt(index), null, MapLayers.ElementAt(index), _actionCounter > 0 ? _currentActionIndex - 1 : _currentActionIndex++));
        }

        void RemoveLayer() {
            if (SelectedMapLayer == null) {
                MessageBox.Show("No layer selected!");
                return;
            } else if (SelectedMapLayer.Id == 0) {
                MessageBox.Show("Can't remove background layer!\nTo clear the grid go to Grid > Clear.");
                return;
            }
            MapLayers.Remove(SelectedMapLayer);
        }

        Bitmap SaveLayerToImage(LayerModel layer) {
            List<Bitmap> images = layer.TileGrid.Select(image => image.MapTileImage).Select(BitmapConverter.ToBitmap).ToList();
            return BitmapConverter.CombineTilesToBitmap(images, _tileCountX, _tileCountY, SPRITE_SIZE);
        }

        void SaveImage() {
            //dialog stuff here maybe
            SaveFileDialog saveFile = new SaveFileDialog();

            saveFile.Title = "Save as";
            //saveFile.AddExtension = true;
            saveFile.FileName = "mapTile-image"; // Default file name
            saveFile.DefaultExt = ".png"; // Default file extension
            saveFile.Filter = "Portable Network Graphic (*.png)|*.png"; // Filter files by extension 
            saveFile.InitialDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).ToString(); // + "\\Data\\"; //todo: implement directory checking first

            var result = saveFile.ShowDialog();

            if (result == true) {
                List<Bitmap> layerMaps = new List<Bitmap>();
                foreach (LayerModel layer in MapLayers) {
                    Bitmap map = SaveLayerToImage(layer);
                    map.Save(Path.GetDirectoryName(saveFile.FileName) + $"/{Path.GetFileNameWithoutExtension(saveFile.SafeFileName)}-{layer.LayerName}.png");
                    layerMaps.Add(map);
                }

                BitmapConverter.MergeLayersToBitmap(layerMaps, MapWidth).Save(Path.GetDirectoryName(saveFile.FileName) + $"/{Path.GetFileNameWithoutExtension(saveFile.SafeFileName)}.png");
            }
        }

        void SetStatus(string text, int value) {
            StatusText = text;
            StatusProgressPercentage = value > 100 ? 100 : value < 0 ? 0 : value;
            RaisePropertyChanged("StatusText");
            RaisePropertyChanged("StatusProgressPercentage");
        }

        private void WorkerDoWork(object sender, DoWorkEventArgs e) {
            // run all background tasks here
            object[] parameters = e.Argument as object[];

            int xSprites = Convert.ToInt32(parameters[0]);
            int ySprites = Convert.ToInt32(parameters[1]);
            BitmapImage spriteSheet = parameters[2] as BitmapImage;
            OpenFileDialog openFile = parameters[3] as OpenFileDialog;

            Random r = new Random();
            int counter = 0;
            for (int i = 0; i < ySprites; i++) {
                for (int j = 0; j < xSprites; j++) {
                    try {

                        if (((j * SPRITE_SIZE) + SPRITE_SIZE) > (int)spriteSheet.Width) continue;
                        if (((i * SPRITE_SIZE) + SPRITE_SIZE) > (int)spriteSheet.Height) continue;

                        BitmapSource bitmapSource = new CroppedBitmap(spriteSheet, new Int32Rect(j * SPRITE_SIZE, i * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE)) as BitmapSource;

                        //Conversion from CroppedBitmap (BitmapSource) to BitmapImage
                        BitmapImage img = BitmapConverter.BitmapSourceToImage(bitmapSource);

                        TileModel x = new TileModel {
                            Id = (uint)r.Next(9999999),
                            MapTileImage = img,
                            Height = SPRITE_SIZE,
                            Width = SPRITE_SIZE,
                            PosX = 0,//(int)widthTotal,
                            PosY = 0,
                            Scale = 1
                        };


                        Application.Current.Dispatcher.Invoke((Action)(() => {
                            MapTiles.Add(x);
                        }));

                    } catch (Exception ex) {
                        // Could not load the image
                        MessageBox.Show("Could not load image: " + openFile.FileName.Substring(openFile.FileName.LastIndexOf('\\')) +
                            "\n\nReported error: " + ex.Message + "\n\nIndex i: " + i * SPRITE_SIZE + "\n\nIndex j: " + j * SPRITE_SIZE +
                            "\n\nMake sure your spritesheet has correct dimentions and a DPI of 96.");
                    }

                    int process = ++counter * 100 / (xSprites * ySprites);
                    _worker.ReportProgress(Convert.ToInt32(process), xSprites * ySprites);
                }
                SetStatus(counter + "/" + xSprites * ySprites, counter * 100 / (xSprites * ySprites));////  250/15323 sprites parsed
                CommandManager.InvalidateRequerySuggested();
                //Application.Current.MainWindow.UpdateLayout();
                //SetStatus((1 + i) * xSprites + "/" + xSprites * ySprites, (xSprites * ySprites) / (1 + i));////  250/15323 sprites parsed
                //Application.DoEvents();               
            }
        }

        private void WorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            //update ui once worker complete his work
            if (!Initialized) InitLayout(_tileCountX, _tileCountY);
            Initialized = true;
            SetStatus("Ready", 100);
            //Save();
        }

        private void ProgressChanged(object sender, ProgressChangedEventArgs e) {
            StatusProgressPercentage = e.ProgressPercentage;
            //RaisePropertyChanged("StatusProgressPercentage");
        }

        void Save() {
            if (MapTiles.Count == 0) {
                SetStatus("Nothing much to save, eh?", 0);
                return;
            }

            SaveFileDialog saveFile = new SaveFileDialog();

            saveFile.Title = "Save as";
            saveFile.AddExtension = true;
            saveFile.FileName = "mapTile-savefile"; // Default file name
            saveFile.DefaultExt = ".bin"; // Default file extension
            saveFile.Filter = "MapEditor Data|*.bin"; // Filter files by extension 
            saveFile.InitialDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).ToString(); // + "\\Data\\"; //todo: implement directory checking first

            var result = saveFile.ShowDialog();

            if (result == true) {
                // string directory = Directory.GetParent(Directory.GetCurrentDirectory()).ToString();

                string[] layernames = MapLayers.Select(map => map.LayerName).ToArray();
                object[] data = { MapWidth, MapHeight, SPRITE_SIZE, ZoomLevel, layernames };
                Serialisation.WriteToBinaryFile(saveFile.FileName, data); //(directory + "/Data/mapTile-savefile.bin"

                //List<Bitmap> layerMaps = new List<Bitmap>();
                Directory.CreateDirectory(Path.GetDirectoryName(saveFile.FileName) + "/Data/");

                foreach (LayerModel layer in MapLayers) {
                    Bitmap map = SaveLayerToImage(layer);
                    map.Save(Path.GetDirectoryName(saveFile.FileName) + $"/Data/{Path.GetFileNameWithoutExtension(saveFile.SafeFileName)}-{layer.LayerName}.png"); //todo data folder checking
                    //layerMaps.Add(map);
                }
            }

            // Load();
        }

        void Load() {

            OpenFileDialog openFile = new OpenFileDialog();

            openFile.Title = "Select save file";
            openFile.Filter = "MapEditor Data|*.bin";

            SetStatus("Select savefile", 0);
            //openFile.InitialDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).ToString();// + "\\Data\\"; //todo: implement directory checking first
            var result = openFile.ShowDialog();

            if (result == true) {
                Parse(openFile.FileName);
                SetStatus("Ready", 100);
                Initialized = true;
            } else SetStatus("Selection canceled", 0);
        }

        void Import() {
            OpenFileDialog openFile = new OpenFileDialog();

            openFile.Title = "Select background image";
            openFile.Filter = "Portable Network Graphic(*.png) | *.png";

            SetStatus("Select background image", 0);
            // openFile.InitialDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).ToString();// + "\\Data\\"; //todo: implement directory checking first
            var result = openFile.ShowDialog();

            if (result == true) {
                MapBackground.Clear();
                Bitmap b = new Bitmap(openFile.FileName);

                BitmapImage spriteSheet = BitmapConverter.BitmapSourceToImage(BitmapConverter.ConvertBitmapTo96DPI(BitmapConverter.ToBitmapImage(b)));

                int xSprites = (int)spriteSheet.Width / SPRITE_SIZE;
                int ySprites = (int)spriteSheet.Height / SPRITE_SIZE;

                SetStatus("Extracting background image", 0);

                Random r = new Random();
                int counter = 0;
                for (int i = 0; i < ySprites; i++) {
                    for (int j = 0; j < xSprites; j++) {
                        try {

                            if (((j * SPRITE_SIZE) + SPRITE_SIZE) > (int)spriteSheet.Width) continue;
                            if (((i * SPRITE_SIZE) + SPRITE_SIZE) > (int)spriteSheet.Height) continue;

                            BitmapSource bitmapSource = new CroppedBitmap(spriteSheet, new Int32Rect(j * SPRITE_SIZE, i * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE)) as BitmapSource;

                            //Conversion from CroppedBitmap (BitmapSource) to BitmapImage
                            BitmapImage img = BitmapConverter.BitmapSourceToImage(bitmapSource);

                            TileModel x = new TileModel {
                                Id = (uint)counter,//(i * j + j),
                                MapTileImage = img,
                                Height = SPRITE_SIZE,
                                Width = SPRITE_SIZE,
                                PosX = j * SPRITE_SIZE,//(int)widthTotal,
                                PosY = i * SPRITE_SIZE,
                                Scale = 1
                            };

                            MapBackground.Add(x);

                        } catch (Exception ex) {
                            // Could not load the image
                            MessageBox.Show("Could not load image: " + openFile.FileName +
                                "\n\nReported error: " + ex.Message + "\n\nIndex i: " + i * SPRITE_SIZE + "\n\nIndex j: " + j * SPRITE_SIZE +
                                "\n\nMake sure your spritesheet has correct dimentions and a DPI of 96.");
                        }

                        //int process = ++counter * 100 / (xSprites * ySprites);
                        ++counter;
                    }
                    SetStatus(counter + "/" + xSprites * ySprites, counter * 100 / (xSprites * ySprites));
                }
                SetStatus("Ready", 100);
                Initialized = true;
            } else SetStatus("Selection canceled", 0);
        }

        private void Parse(string filename) {
            Initialized = false;
            //MapLayers = Serialisation.ReadFromBinaryFile<ObservableCollection<LayerModel>>(Directory.GetCurrentDirectory() + "mapTile-savefile.bin");
            object[] data = Serialisation.ReadFromBinaryFile<object[]>(filename); //Directory.GetParent(Directory.GetCurrentDirectory()) +"/Data/mapTile-savefile.bin"

            int mapwidth = int.Parse(data[0].ToString());
            int mapheigth = int.Parse(data[1].ToString());
            int spritesize = int.Parse(data[2].ToString());
            float zoomlvl = float.Parse(data[3].ToString());
            object[] layernames = data[4] as object[];

            MapWidth = mapwidth;
            MapHeight = mapheigth;
            //SPRITE_SIZE = spritesize; //todo: add when not const
            ZoomLevel = zoomlvl;

            string[] layers = layernames?.Where(x => x != null).Select(x => x.ToString()).ToArray();

            MapBackground.Clear();
            MapLayers.Clear();
            foreach (var layer in layers) {
                Bitmap b;
                try {
                    b = new Bitmap(Path.GetDirectoryName(filename) + $"\\Data\\{Path.GetFileNameWithoutExtension(filename)}-{layer}.png");
                }
                catch (Exception e) {
                    MessageBox.Show("Save file corrupted!");
                    return;
                }
                BitmapImage spriteSheet = BitmapConverter.BitmapSourceToImage(BitmapConverter.ConvertBitmapTo96DPI(BitmapConverter.ToBitmapImage(b)));

                int xSprites = (int)spriteSheet.Width / SPRITE_SIZE;
                int ySprites = (int)spriteSheet.Height / SPRITE_SIZE;

                SetStatus("Extracting spritesheet", 0);

                Random r = new Random();
                int counter = 0;
                for (int i = 0; i < ySprites; i++) {
                    for (int j = 0; j < xSprites; j++) {
                        try {

                            if (((j * SPRITE_SIZE) + SPRITE_SIZE) > (int)spriteSheet.Width) continue;
                            if (((i * SPRITE_SIZE) + SPRITE_SIZE) > (int)spriteSheet.Height) continue;

                            BitmapSource bitmapSource = new CroppedBitmap(spriteSheet, new Int32Rect(j * SPRITE_SIZE, i * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE)) as BitmapSource;

                            //Conversion from CroppedBitmap (BitmapSource) to BitmapImage
                            BitmapImage img = BitmapConverter.BitmapSourceToImage(bitmapSource);

                            TileModel x = new TileModel {
                                Id = (uint)counter,//(i * j + j),
                                MapTileImage = img,
                                Height = SPRITE_SIZE,
                                Width = SPRITE_SIZE,
                                PosX = j * SPRITE_SIZE,//(int)widthTotal,
                                PosY = i * SPRITE_SIZE,
                                Scale = 1
                            };

                            MapBackground.Add(x);

                        } catch (Exception ex) {
                            // Could not load the image
                            MessageBox.Show("Could not load image: " + filename.Substring(filename.LastIndexOf('\\')) +
                                "\n\nReported error: " + ex.Message + "\n\nIndex i: " + i * SPRITE_SIZE + "\n\nIndex j: " + j * SPRITE_SIZE +
                                "\n\nMake sure your spritesheet has correct dimentions and a DPI of 96.");
                        }

                        //int process = ++counter * 100 / (xSprites * ySprites);
                        ++counter;
                    }
                    SetStatus(counter + "/" + xSprites * ySprites, counter * 100 / (xSprites * ySprites));
                }

                MapLayers.Add(new LayerModel { TileGrid = new ObservableCollection<TileModel>(MapBackground), Id = 0, LayerName = layer, Enabled = true });
                MapBackground.Clear();
            }
            MapBackground.Clear();
            foreach (TileModel tile in MapLayers.ToList().Find(layer => layer.LayerName == "BackgroundLayer").TileGrid) {
                MapBackground.Add(tile);
            }
        }

        void Undo() {
            if (_undoStack.Count == 0) return;

            long startActionIndex = _undoStack.Peek().ActionIndex;
            long newActionIndex = ++_currentActionIndex;

            while (_undoStack.Count != 0 && _undoStack.Peek().ActionIndex == startActionIndex) {
                SpriteAction lastAction = _undoStack.Pop();
                if (lastAction.ActionType == "Draw Tile") {
                    _redoStack.Push(new SpriteAction(lastAction.Tile, lastAction.PreviousBitmap, lastAction.NextBitmap, newActionIndex));
                    lastAction.Tile.MapTileImage = lastAction.PreviousBitmap;
                } else if (lastAction.ActionType == "Create Layer") {
                    _redoStack.Push(new SpriteAction(lastAction.Layer, lastAction.PreviousLayer, lastAction.NextLayer, newActionIndex));
                    //lastAction.Layer = lastAction.PreviousLayer;
                    if (lastAction.PreviousLayer == null) {
                        SelectedMapLayer = lastAction.Layer;
                        MapLayers.Remove(SelectedMapLayer);
                    } else {
                        MapLayers.Add(lastAction.NextLayer);
                    }
                }
            }
        }

        void Redo() {
            if (_redoStack.Count == 0) return;

            long startActionIndex = _redoStack.Peek().ActionIndex;
            long newActionIndex = ++_currentActionIndex;

            while (_redoStack.Count != 0 && _redoStack.Peek().ActionIndex == startActionIndex) {
                SpriteAction nextAction = _redoStack.Pop();
                if (nextAction.ActionType == "Draw Tile") {
                    _undoStack.Push(new SpriteAction(nextAction.Tile, nextAction.PreviousBitmap, nextAction.NextBitmap, newActionIndex));
                    nextAction.Tile.MapTileImage = nextAction.NextBitmap;
                } else if (nextAction.ActionType == "Create Layer") {
                    _undoStack.Push(new SpriteAction(nextAction.Layer, nextAction.PreviousLayer, nextAction.NextLayer, newActionIndex));
                    if (nextAction.NextLayer == null) {
                        SelectedMapLayer = nextAction.Layer;
                        MapLayers.Remove(SelectedMapLayer);
                    } else {
                        MapLayers.Add(nextAction.NextLayer);
                    }
                    //nextAction.Layer = nextAction.NextLayer;
                }

            }
        }

        void Reset() {
            ZoomLevel = 1f;

            MapTiles.Clear();
            MapLayers.Clear();
            MapBackground.Clear();
            SelectedMapLayer = null;

            MapWidth = 16 * SPRITE_SIZE;
            MapHeight = 16 * SPRITE_SIZE;

            InitLayout(_tileCountX, _tileCountY);
            MapLayers.Add(new LayerModel { TileGrid = MapBackground, Id = 0, LayerName = "BackgroundLayer", Enabled = true });
            SelectedMapLayer = MapLayers[0];
            SetStatus("Load a file to start", 0);
        }

        void GridResizeUniform(int newSize) {
            if(newSize == 1) GridResize((MapWidth / SPRITE_SIZE) + 1, (MapHeight / SPRITE_SIZE) + 1);
            else if (newSize == -1) GridResize((MapWidth / SPRITE_SIZE) - 1, (MapHeight / SPRITE_SIZE) - 1);
            else GridResize(newSize, newSize);
        }

        void GridResize(int x, int y) {
  
            int oldsizeX = MapWidth;
            int oldsizeY = MapHeight;

            MapWidth = x * SPRITE_SIZE;
            MapHeight = y * SPRITE_SIZE;

            List<LayerModel> mapLayers = MapLayers.ToList();

            List<TileModel> background = MapBackground.ToList();
            //MapLayers
            //MapBackground
            InitLayout(_tileCountX, _tileCountY);

            foreach (TileModel tile in MapBackground) {
                if (tile.PosX >= oldsizeX || tile.PosY >= oldsizeY) continue;

                var img = background.Where(t => t.PosX == tile.PosX && t.PosY == tile.PosY).Select(t => t.MapTileImage).ElementAt(0);
                if (img == null) continue;
                tile.MapTileImage = img;
            }

            int layerCount = MapLayers.Count - 1;


            MapLayers.Clear();
            MapLayers.Add(new LayerModel { TileGrid = MapBackground, Id = 0, LayerName = "BackgroundLayer", Enabled = true });
            for (int i = 0; i < layerCount; i++) {
                AddLayer();
            }

            foreach (LayerModel layer in MapLayers) {
                if(layer.LayerName == "BackgroundLayer") continue;

                foreach (TileModel tile in layer.TileGrid) {
                    if (tile.PosX >= oldsizeX || tile.PosY >= oldsizeY) continue;

                    var img = mapLayers.Where(l => l.LayerName == layer.LayerName).Select(l => l.TileGrid).ElementAt(0).Where(t => t.PosX == tile.PosX && t.PosY == tile.PosY).Select(t => t.MapTileImage).ElementAt(0);
                    if (img == null) continue;
                    tile.MapTileImage = img;
                }
            }

        }

        void Eraser() {           
            SelectedMapTile = new TileModel { MapTileImage = BitmapConverter.ToBitmapImage(BitmapConverter.EmptyTile(SPRITE_SIZE)) };
        }

        void Fill() {
            if (MapLayers.Count == 0 || SelectedMapLayer == null) return;
            _actionCounter = 0;

            foreach (TileModel t in MapLayers.ElementAt(MapLayers.IndexOf(SelectedMapLayer)).TileGrid) {
                UpdateTile(t);
            }
        }
    }
}