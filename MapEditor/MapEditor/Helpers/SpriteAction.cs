﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using MapEditor.Model;

namespace MapEditor.Helpers {
    public class SpriteAction {
        public TileModel Tile { get; set; }
        public BitmapImage PreviousBitmap { get; set; }
        public BitmapImage NextBitmap { get; set; }
        public long ActionIndex {get; set;}
        public LayerModel Layer { get; set; }
        public LayerModel PreviousLayer { get; set; }
        public LayerModel NextLayer { get; set; }
        public string ActionType { get; set; }

        public SpriteAction(TileModel tile, BitmapImage prev, BitmapImage next, long actionIndex) {
            Tile = tile;
            PreviousBitmap = prev;
            NextBitmap = next;
            ActionIndex = actionIndex;
            ActionType = "Draw Tile";
        }

        public SpriteAction(LayerModel layerModel, LayerModel prev, LayerModel next, long actionIndex) {
            Layer = layerModel;
            PreviousLayer = prev;
            NextLayer = next;
            ActionIndex = actionIndex;
            ActionType = "Create Layer";
        }
    }
}
