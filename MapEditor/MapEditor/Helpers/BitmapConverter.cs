﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
namespace MapEditor.Helpers {
    static class BitmapConverter {

        public static BitmapSource ConvertBitmapTo96DPI(BitmapImage bitmapImage) {
            double dpi = 96;
            int width = bitmapImage.PixelWidth;
            int height = bitmapImage.PixelHeight;

            int stride = width * bitmapImage.Format.BitsPerPixel;
            byte[] pixelData = new byte[stride * height];
            bitmapImage.CopyPixels(pixelData, stride, 0);

            return BitmapSource.Create(width, height, dpi, dpi, bitmapImage.Format, null, pixelData, stride);
        }

        public static void ResizeImage(System.Windows.Controls.Image img, double maxWidth, double maxHeight) {
            if (img == null || img.Source == null)
                return;

            double srcWidth = img.Source.Width;
            double srcHeight = img.Source.Height;

            // Set your image tag to the sources DPI value for smart resizing if DPI != 96
            if (img.Tag != null && img.Tag.GetType() == typeof(double[])) {
                double[] DPI = (double[])img.Tag;
                srcWidth = srcWidth / (96 / DPI[0]);
                srcHeight = srcHeight / (96 / DPI[1]);
            }

            double resizedWidth = srcWidth;
            double resizedHeight = srcHeight;

            double aspect = srcWidth / srcHeight;

            if (resizedWidth > maxWidth) {
                resizedWidth = maxWidth;
                resizedHeight = resizedWidth / aspect;
            }
            if (resizedHeight > maxHeight) {
                aspect = resizedWidth / resizedHeight;
                resizedHeight = maxHeight;
                resizedWidth = resizedHeight * aspect;
            }

            img.Width = resizedWidth;
            img.Height = resizedHeight;
        }

        public static BitmapImage BitmapSourceToImage(BitmapSource bitmapSource) {
            //Loses aplha channel

            /*//Conversion from CroppedBitmap (BitmapSource) to BitmapImage
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            MemoryStream memoryStream = new MemoryStream();
            BitmapImage img = new BitmapImage();

            encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
            encoder.Save(memoryStream);

            img.BeginInit();
            img.StreamSource = new MemoryStream(memoryStream.ToArray());
            img.EndInit();

            memoryStream.Close();
            //End conversion

            return img;*/

            //convert image format
            var src = new System.Windows.Media.Imaging.FormatConvertedBitmap();
            src.BeginInit();
            src.Source = bitmapSource;
            src.DestinationFormat = System.Windows.Media.PixelFormats.Bgra32;
            src.EndInit();

            //copy to bitmap
            Bitmap bitmap = new Bitmap(src.PixelWidth, src.PixelHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            var data = bitmap.LockBits(new Rectangle(Point.Empty, bitmap.Size), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            src.CopyPixels(System.Windows.Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
            bitmap.UnlockBits(data);

            return ToBitmapImage(bitmap);
        }

        public static BitmapImage ToBitmapImage(Bitmap bitmap) {
            using (MemoryStream stream = new MemoryStream()) {
                bitmap.Save(stream, ImageFormat.Png);

                stream.Position = 0;
                BitmapImage result = new BitmapImage();
                result.BeginInit();
                // According to MSDN, "The default OnDemand cache option retains access to the stream until the image is needed."
                // Force the bitmap to load right now so we can dispose the stream.
                result.CacheOption = BitmapCacheOption.OnLoad;
                result.StreamSource = stream;
                result.EndInit();
                result.Freeze();
                return result;
            }
        }

        public static Bitmap ToBitmap(BitmapImage bitmapImage) {
            // BitmapImage bitmapImage = new BitmapImage(new Uri("../Images/test.png", UriKind.Relative));
            if (bitmapImage == null) return null;
            using (MemoryStream outStream = new MemoryStream()) {
                BitmapEncoder enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapImage));
                enc.Save(outStream);
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(outStream);

                return new Bitmap(bitmap);
            }
        }

        public static Bitmap CombineTilesToBitmap(List<Bitmap> images, int widthMap, int heightMap, int spriteSize) { //string[] files
           Bitmap finalImage = null;

            try {
                //create a bitmap to hold the combined image
                finalImage = new Bitmap(widthMap * spriteSize, heightMap * spriteSize);

                //get a graphics object from the image so we can draw on it
                using (Graphics g = Graphics.FromImage(finalImage)) {
                    //set background color
                    g.Clear(Color.Transparent);

                    //go through each image and draw it on the final image
                    int offsetX = 0, offsetY = 0;
                    foreach (Bitmap image in images) {
                        if(image!= null) g.DrawImage(image, new Rectangle(offsetX, offsetY, spriteSize, spriteSize));
                        offsetX += spriteSize;

                        if (offsetX != widthMap * spriteSize) continue;
                        offsetX = 0;
                        offsetY += spriteSize;
                    }
                }
                return finalImage;
            } catch (Exception ex) {
                finalImage?.Dispose();

                throw ex;
            } finally {
                //clean up memory
                foreach (Bitmap image in images) {
                    image?.Dispose();
                }
            }
        }

        public static Bitmap MergeLayersToBitmap(List<Bitmap> images, int mapSize) {
            Bitmap finalImage = null;

            try {
                //create a bitmap to hold the combined image
                finalImage = new Bitmap(mapSize, mapSize);

                //get a graphics object from the image so we can draw on it
                using (Graphics g = Graphics.FromImage(finalImage)) {
                    //set background color
                    g.Clear(Color.Transparent);

                    //go through each image and draw it on the final image
                    foreach (Bitmap image in images.Where(image => image != null)) {
                        g.DrawImage(image, new Rectangle(0, 0, mapSize, mapSize));
                    }
                }

                return finalImage;
            } catch (Exception) {
                finalImage?.Dispose();

                throw;
            } finally {
                //clean up memory
                foreach (Bitmap image in images) {
                    image?.Dispose();
                }
            }
        }

        public static Bitmap EmptyTile(int spriteSize) {         
            Bitmap bitmap = new Bitmap(spriteSize, spriteSize);
            for (int x = 0; x < bitmap.Height; ++x) {
                for (int y = 0; y < bitmap.Width; ++y) {
                    bitmap.SetPixel(x, y, Color.FromArgb(0, 255, 255, 255));
                }
            }
            for (int x = 0; x < bitmap.Height; ++x) {
                bitmap.SetPixel(x, x, Color.FromArgb(0, 255, 255, 255));
            }
            return bitmap;
        }
    }
}
