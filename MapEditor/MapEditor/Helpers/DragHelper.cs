﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using MapEditor.Model;

namespace MapEditor.Helpers {
    public class DragHelper {
        public TileModel TileParam { get; set; }
        public MouseButtonEventArgs MouseParam { get; set; }
    }
}
